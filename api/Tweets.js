/**
 * Mocking client-server processing. TODO!
 */
let ItemId = 1

const items = [
  {
    'id': ItemId++
  }
]

export default {
  getTweets (cb) {
    setTimeout(() => cb(items), 500)
  },

  postTweet (items, cb, errorCb) {
    setTimeout(() => {
      // simulate random checkout failure.
      (Math.random() > 0.5 || navigator.userAgent.indexOf('PhantomJS') > -1)
        ? cb()
        : errorCb()
    }, 100)
  }
}
