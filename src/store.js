import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    tweets: [],
    users: ['George', 'Matan', 'Ohad'],
    user: null,
    search: ''
  },
  mutations: {
    setTweets (state, tweets) {
      state.tweets = tweets
    },
    setUser (state, user) {
      state.user = user
    },
    searchTweet (state, text) {
      state.search = text
    },
    addTweet ({ tweets }, text) {
      tweets.push({
        text,
        completed: false,
        edit: false,
        liked: false, // TODO
        likes: 0,
        user: this.state.user,
        date: new Date()
      })
    },
    editTweet: ({ tweets }, tweet) => {
      tweets[tweets.indexOf(tweet)].text = tweet.text
      tweets[tweets.indexOf(tweet)].edit = !tweet.edit
    },
    removeTweet: ({ tweets }, tweet) => {
      tweets.splice(tweets.indexOf(tweet), 1)
    },
    likeTweet (tweets, { tweet, user }) {
      // TODO
    }
  },
  getters: {
    tweets: (state, getters) => getters.filteredTweets.filter(function (mode) {
      return mode.text.toLowerCase().indexOf(state.search.toLowerCase()) >= 0 ||
        mode.user.toLowerCase().indexOf(state.search.toLowerCase()) >= 0
    }),
    filteredTweets: state => state.tweets.sort(function (a, b) {
      return new Date(b.date) - new Date(a.date)
    }),
    currentUser: state => state.user,
    allUsers: state => state.users
  }
})
